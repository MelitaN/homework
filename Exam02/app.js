function BookStore(name){
    this.name = name;
    this.books = [];
    this.shoppingCard = [];//[ [0] [1] [2] [3]]
    this.wishList = [];

    //Adding to the books array
    this.addBook = function(book){
        this.books.push(book);
    }

    //Listing the books
    this.listBooks = function(){
        let element = document.getElementById('books-list');
        let html = '';
        let index = 0;
        for(let book of this.books){
        html +=
        `<li id="1" class="card" data-index=${index}>
                        <div class="card-title">${book.title} </div>
                         <div class="card-img"> <img src="${book.image}"> </div>
                         <div class="card-description"> ${book.description} </div>
                         <div class="card-price"> Price: ${book.price}</div>
                         <div class="card-quantity"> Quantity: ${book.quantity} </div>
                         <div class="card-actions"> 
                            <button class="buy-btn btn buy"> buy </button> 
                            <button class="wishlist-btn btn"> add to wishlist </button>
                         </div>
                         </li>`;
                         index++;
                        }
        element.innerHTML = html;
    }

    //add to shopping cart
    this.addToShoppingCard = function(book){
        book.shoppingIndex = this.shoppingCard.length;
        this.shoppingCard.push(book);
        this.listShoppingCard();
    }

    //Listing the shopping cart 
    this.listShoppingCard = function(){
        const element = document.getElementById('shopping-cart');
        let index = 0;
        let html = '';
        for(let book of this.shoppingCard){
            html += `<li id="1" data-index="${index}"> ${book.title} ${book.author} ${book.price}  <button class="remove-book-card" id="1"> remove </button> </li>`
            index++;
        }
        element.innerHTML = html;
        
    }

    //To get the index of the element
    this.getIndex = function(index){
        let book = this.books[index];
        if(book){
            return book;
        }
        else{
            return false;
        }
    }

    //Add to wishlist
    this.addToWishlist = function(book){
        this.wishList.push(book);
        this.listWishList();
    }

    //Listing of the wishlist
    this.listWishList = function(){
        const element = document.getElementById('wishlist');
        let index = 0;
        let html = '';
        for(let book of this.wishList){
            html += `<li id="1" data-index="${index}"> ${book.title} ${book.author} ${book.price}  <button class="remove-book-cart" id="1"> remove </button> </li>`
            index++;
        }
        element.innerHTML = html;
    }



}

function Book(title, author, image, price, quantity, description){
    this.title = title;
    this.author = author;
    this.image = image;
    this.price = price;
    this.quantity = quantity;
    this.description = description;
}

const bookStore = new BookStore("MN Knizara");
//ADD BUTTON
const addBtn = document.getElementById('add-btn');
addBtn.addEventListener('click', function(e){
    e.preventDefault();

    let title = document.getElementById('title').value;
    let author = document.getElementById('author').value;
    let image = document.getElementById('thumbnail').value;
    let price = document.getElementById('price').value;
    let quantity = document.getElementById('quantity').value;
    let description = document.getElementById('description').value;

    let newBooks = new Book(title, author, image, price, quantity, description);
    bookStore.addBook(newBooks);
    bookStore.listBooks();

})

//BUY BUTTTON
document.addEventListener('click', function(e){
    if(e.target.classList.contains('buy-btn')){
        let dataIndex = parseInt(e.target.closest('li').getAttribute('data-index'));
        let book = bookStore.getIndex(dataIndex);
        bookStore.addToShoppingCard(book); 
        book.quantity -= 1;
        bookStore.listBooks();
        if (book.quantity === 0){
           
        }
    }
    })

//ADD TO WISHLIST BUTTON
document.addEventListener('click', function(e){
    if(e.target.classList.contains('wishlist-btn')){
        let dataIndex = parseInt(e.target.closest('li').getAttribute('data-index'));
        let book = bookStore.getIndex(dataIndex);
        if(book){
            bookStore.addToWishlist(book);
        }
    }
})




document.addEventListener('click', function(e) {
    if(e.target.classList.contains('remove-book-card')) {
        let index = parseInt(e.target.closest('li').getAttribute('data-index'));
        let index1 = bookStore.shoppingCard[index];  //1
        let position = bookStore.shoppingCard.indexOf(index1);//shopping card [ [0],[1],[2] ]
        bookStore.shoppingCard.splice(position, 1);
        bookStore.listShoppingCard();
    } 
})

document.addEventListener('click', function(e) {
    if(e.target.classList.contains('remove-book-cart')) {
        let index = parseInt(e.target.closest('li').getAttribute('data-index'));
        let index1 = bookStore.wishList[index];
        let position = bookStore.wishList.indexOf(index1);
        bookStore.wishList.splice(position, 1);
        bookStore.listWishList();
    } 
})




