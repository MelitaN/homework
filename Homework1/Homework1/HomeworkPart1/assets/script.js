//First paragraph
let firstParagraph = document.getElementsByClassName("paragraph")[0];
console.log(firstParagraph);
firstParagraph.innerText = "This is my first paragraph";

//Second paragraph
let secondParagraph = document.getElementsByClassName("second")[0];
console.log(secondParagraph)
secondParagraph.innerText ="This is my second paragraph"

//First header
let firstHeader = document.getElementById("myTitle");
console.log(firstHeader);
firstHeader.innerHTML ="This is my first header"

//Second header
let secondHeader = document.getElementsByTagName("h1")[1];
console.log(secondHeader);
secondHeader.innerText ="This is the second header"

//The last header 
let lastHeader = document.getElementsByTagName("h3")[0];
console.log(lastHeader);
lastHeader.innerText += " I will not change this text, i will just add some new text"

/*
ONE MORE SOLUTION 

let lastDiv = document.getElementsByTagName("body")[0].lastElementChild;
lastDiv=lastDiv.previousElementSibling;
console.log(lastDiv)
let secondHeader1 = lastDiv.firstElementChild;
console.log(secondHeader1);
secondHeader1.innerText = "This is the second headerr";
let thirdHeader = lastDiv.lastElementChild;
console.log(thirdHeader);
thirdHeader.innerText += " Let's again add some new text"*/
